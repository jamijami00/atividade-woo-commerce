<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'E+SKn1cbUnPNlKOonhck59vc1TP7YxRihRy4WFMWoDVVoQHBoUVO8bixuyEmen6v7MqfF6NOXDaIOw1YPSOrYA==');
define('SECURE_AUTH_KEY',  '5DI5DeWemxs7Ct/pH62cXO0q/FUXE6Pj0W2q0OSofjpBJSD9t3MuC3R8OVZSNQDEXg+0Ru+GqL5LTLlj+ISqOQ==');
define('LOGGED_IN_KEY',    'IUv00nB568It/hrWyQmC84rMMhX2D3X2qONbM2lbpKKpw7CY7dF+lrWYlzSceWf4UCMpxgX1GT2nfTij2scXJA==');
define('NONCE_KEY',        'PZubKqnAkCv853bJN00x+HVf4L63LvozQYSYohepJ0lsayiOsr4SuJZX0wAQaz9SnwwOBG9/M3a9yYuKBOn7QA==');
define('AUTH_SALT',        'aWObm3rBvGa3tMbf2lAByMRmh+Dq/OqGZEH+d7/WsTm0d8tPwG2I7o1nF2l6O5m7GZmHqjiR220hdjFZBNr6BQ==');
define('SECURE_AUTH_SALT', 'GHFmiHNR36vx7RBjHDi3dq6DCnpeOnTkz8igLhPHb5fs5Yq6Sl1JPVKimfB1Y9kcEKEAIQKrQ6mEo59Tz87VOg==');
define('LOGGED_IN_SALT',   'TNLjJ9Mvo0BBHpB3rAWnAop9o+QA5XlA9rMzHGtrUeLnBUlwP6p3pOr3odVhOMvZoX+1HBlEoNw4+pbeDwlgvw==');
define('NONCE_SALT',       'P19TGPxnZCanSsPCccdFMjDOUU5H8DxUCi/XpNxnBHuEuW7EcV8kklP9FSdMtEC2Tj0nmpWrS8PKRgJb7lsHnw==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
